(function ($) {
  Drupal.behaviors.YandexTranslate = {
    attach: function (context, settings) {

     $('select.yandex-translate').change(function(){
       var link = $(this);
       var lang = link.val();
       var translating = '<span class="translating">' + Drupal.t('Translating...') + '</span>';
       var field = $(this).prevAll('.form-item').find('textarea, input[type=text]');
       if(field.length) {
         if(lang == 0) {
           if(field.data('original-text').length) {
             field.val(field.data('original-text'));
           }
         }
         else { 
           var text = field.val();
           field.attr('data-original-text', text);
           if(text.length) {
             var postData = {
               text:text,
               key:settings.yandexTranslate.key,
               lang:lang,
               format:settings.yandexTranslate.format,
             };

             $.ajax({
               url : settings.yandexTranslate.url,
               type: "POST",
               data : postData,
               beforeSend: function() {
                 link.hide().after(translating);
               },
               complete: function() {
                 $('span.translating').remove();
                 link.show();
               },
               success: function(data, textStatus, jqXHR){
                 if('text' in data) {
                   var translated = data.text.join();
                   field.val(translated);
                 }
               },
             });
           }
         }
       }
       return false;
     });
    }
  }
})(jQuery);