<?php
/**
 * @file
 * Enables the use for alter admin form, notify user for content change. 
 */
/*
 * Admin setting for translation
 * Implements hook_form().
 */
function yandex_translate_settings_form($form, $form_state) {
$form = array();
  $form['yandex_translate_form'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Yandex Translate Setting'),
  ); 
  $key = variable_get('yandex_translate_api_key', 'trnsl.1.1.20140122T113225Z.e576efb32674e35a.e68733910f2ab8e35594a5d42cfdf463bd28e678');
  
  $form['yandex_translate_form']['yandex_translate_api_key'] = array(
    '#title' => t('Api key'),
    '#type' => 'textfield',
    '#default_value' => $key,
    '#required' => TRUE,
    '#description' => t('Your registered API key you got at http://api.yandex.com'),
  );
  
  $langs = array();
  
  if($key) {
   $url = url(YANDEX_TRANSLATE_URL . '/getLangs', array('query' => array('key' => $key)));
   $request = drupal_http_request($url);
    if(empty($request->error) && !empty($request->data)) {
      $list = json_decode($request->data, TRUE);	  	
      if(!empty($list['dirs'])) {
        $langs = drupal_map_assoc(array_map('check_plain', $list['dirs']));
      }
    }
  }
  
  $form['yandex_translate_form']['yandex_translate_translation_directions'] = array(
    '#title' => t('Translation directions'),
    '#type' => 'select',
    '#options' => $langs,
    '#default_value' => variable_get('yandex_translate_translation_directions', array()),
    '#multiple' => TRUE,
    '#required' => TRUE,
  );
  
  $form['yandex_translate_form']['yandex_translate_text_format'] = array(
    '#title' => t('Text format'),
    '#type' => 'select',
    '#options' => array('plain' => t('Plain'), 'html' => 'HTML'),
    '#default_value' => variable_get('yandex_translate_text_format', 'plain'),
  );
  
  $form['yandex_translate_form']['yandex_translate_auto_switch_language'] = array(
    '#title' => t('Switch language automatically during node translation.'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('yandex_translate_auto_switch_language', 1),
    '#access' => module_exists('entity_translation'),
  );  
  $form['yandex_translate_form']['content_type'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Yandex Translation Setting for specific content type and field'),
	'#description' => t('Select Form elements where you want to enable translation.'),
  ); 
   $form_id = array();
  foreach (node_type_get_types() as $type) {
	$form_id[$type->type . '_node_form'] = $type->name;
  }
  $form['yandex_translate_form']['content_type']['form_ids'] = array(
		'#type' => 'select', 
		'#title' => t('Select content type'),
		'#default_value' => variable_get('form_ids', ''),		
        '#options' =>  $form_id,		
		'#weight'      => 6,
        '#rows'        => 10,
		'#column'        => 100,
        '#multiple'    => TRUE,
		'#required' => TRUE,  
	);
	
	
$form['yandex_translate_form']['content_type']['yandex_translate_title_field'] = array(
    '#title' => t('Title.'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('yandex_translate_title_field', 1),    
	//'#weight'      => 7,
  );
  $form['yandex_translate_form']['content_type']['yandex_translate_body_field'] = array(
    '#title' => t('Body.'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('yandex_translate_body_field', 1),  
	//'#weight'   => 8,	
  );

return system_settings_form($form);
 
}
